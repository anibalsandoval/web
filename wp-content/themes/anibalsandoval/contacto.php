<?php
/*
Template Name: contacto
*/
get_header(); ?>

<div class="row">
	<div class="small-12 large-8 columns" role="main">

	<?php if ( have_posts() ) : ?>

		<?php do_action('foundationPress_before_content'); ?>
	<?php query_posts('p=25'); ?>
		<?php while ( have_posts() ) : the_post(); ?>
		
				<?php the_content(); ?>
		<?php endwhile; ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>

		<?php do_action('foundationPress_before_pagination'); ?>

	<?php endif;?>



	<?php do_action('foundationPress_after_content'); ?>


<?php get_footer(); ?>

