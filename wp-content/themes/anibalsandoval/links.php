<?php
/*
Template Name: links
*/
get_header(); ?>
<div class="medium-10 offset-2 columns maso">
	<div class="small-12 large-12 columns" role="main">

	<?php if ( have_posts() ) : ?>

		<?php do_action('foundationPress_before_content'); ?>

		

<ul id="container" class="small-12 columns">		
<?
 $the_query =  new WP_Query( array( 'post_type' => 'links', 'posts_per_page' => -1 ) );
 while( $the_query->have_posts() ) : $the_query->the_post();
?>  


	<li>
 <?php the_content(); ?>
		
	</li>



<?      
 endwhile;
?>
</ul>

		<?php do_action('foundationPress_before_pagination'); ?>

	<?php endif;?>


	<?php do_action('foundationPress_after_content'); ?>

	</div>

</div>
<?php get_footer(); ?>


  <script src="js/vendor/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
    <script>
      $(document).foundation();
var container = document.querySelector('#container');
var msnry = new Masonry( container, {
  // options
  itemSelector: '#container li'
});
</script>
