<?php
/*
Template Name: proyectos
*/
get_header(); ?>
<div class="medium-10 offset-2 columns">
	<div class="small-12 large-12 columns" role="main">

	<?php if ( have_posts() ) : ?>

		<?php do_action('foundationPress_before_content'); ?>

		
<div class="proyect small-12 columns">			
<?
 $the_query =  new WP_Query( array( 'post_type' => 'proyecto', 'posts_per_page' => -1 ) );
 while( $the_query->have_posts() ) : $the_query->the_post();
?>  


	<div class="cada small-12 large-6 columns">

<?php the_post_thumbnail('large'); ?>

<a href="#" data-reveal-id="myModal"><?php the_title(); ?></a>

<div id="myModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <?php the_content(); ?>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

</div>


<?      
 endwhile;
?>
</div>

		<?php do_action('foundationPress_before_pagination'); ?>

	<?php endif;?>


	<?php do_action('foundationPress_after_content'); ?>

	</div>

</div>
<?php get_footer(); ?>
