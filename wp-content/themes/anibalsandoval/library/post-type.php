<?php

function create_post_type () {
  register_post_type('proyecto', 
    array('labels' => 
      array(
        'name' => __('proyectos'),
        'singular_name' => __('proyectos')
      ),
      'public' => true,
      'menu_position' => 4,
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','categories','post-formats'), 
       'taxonomies' => array('category') 
    )
  );

  register_post_type('bitacora', 
    array('labels' => 
      array(
        'name' => __('bitacora '),
        'singular_name' => __('bitacora')
      ),
      'public' => true,
      'menu_position' => 4,
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','categories','post-formats') 
    )
  );

  register_post_type('links', 
    array('labels' => 
      array(
        'name' => __(' links'),
        'singular_name' => __(' links')
      ),
      'public' => true,
      'menu_position' => 4,
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','categories','post-formats'),
      'taxonomies' => array('category')  
    
    )
  );

  register_post_type('performance', 
    array('labels' => 
      array(
        'name' => __('performance'),
        'singular_name' => __('performance')
      ),
      'public' => true,
      'menu_position' => 4,
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','categories','post-formats'),
      'taxonomies' => array('category') 
    )
  );

  // register_post_type('vinedos-slider', 
  //   array('labels' => 
  //     array(
  //       'name' => __('viñedos slider'),
  //       'singular_name' => __('viñedos slider')
  //     ),
  //     'public' => true,
  //     'menu_position' => 4,
  //     'supports' => array('title','editor','author','thumbnail','excerpt','comments','categories','post-formats') 
  //   )
  // );

  // register_post_type('bodega-slider', 
  //   array('labels' => 
  //     array(
  //       'name' => __('bodega slider'),
  //       'singular_name' => __('bodega slider')
  //     ),
  //     'public' => true,
  //     'menu_position' => 4,
  //     'supports' => array('title','editor','author','thumbnail','excerpt','comments','categories','post-formats') 
  //   )
  // );

  flush_rewrite_rules();
}

add_action('init', 'create_post_type');


add_action( 'admin_menu', 'my_create_post_meta_box' );
add_action( 'save_post', 'my_save_post_meta_box', 10, 2 );

function my_create_post_meta_box() {
  add_meta_box( 'my-meta-box', 'Second Excerpt', 'my_post_meta_box', 'post', 'normal', 'high' );
}

function my_post_meta_box( $object, $box ) { ?>
  <p>
    <label for="second-excerpt">Second Excerpt</label>
    <br />
    <textarea name="second-excerpt" id="second-excerpt" cols="60" rows="4" tabindex="30" style="width: 97%;"><?php echo wp_specialchars( get_post_meta( $object->ID, 'Second Excerpt', true ), 1 ); ?></textarea>
    <input type="hidden" name="my_meta_box_nonce" value="<?php echo wp_create_nonce( plugin_basename( __FILE__ ) ); ?>" />
  </p>
<?php }

function my_save_post_meta_box( $post_id, $post ) {

  if ( !wp_verify_nonce( $_POST['my_meta_box_nonce'], plugin_basename( __FILE__ ) ) )
    return $post_id;

  if ( !current_user_can( 'edit_post', $post_id ) )
    return $post_id;

  $meta_value = get_post_meta( $post_id, 'Second Excerpt', true );
  $new_meta_value = stripslashes( $_POST['second-excerpt'] );

  if ( $new_meta_value && '' == $meta_value )
    add_post_meta( $post_id, 'Second Excerpt', $new_meta_value, true );

  elseif ( $new_meta_value != $meta_value )
    update_post_meta( $post_id, 'Second Excerpt', $new_meta_value );

  elseif ( '' == $new_meta_value && $meta_value )
    delete_post_meta( $post_id, 'Second Excerpt', $meta_value );
}




?>